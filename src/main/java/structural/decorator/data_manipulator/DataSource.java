package structural.decorator.data_manipulator;

public interface DataSource {
    void writeData(String data);
    String readData();

}
