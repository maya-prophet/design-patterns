package structural.decorator.data_manipulator;

import java.io.*;

public class FileDataSource implements DataSource {

    private String name;

    public FileDataSource(String name) {
        this.name = name;
    }

    @Override
    public void writeData(String data) {
        File file = new File(name);
        try(OutputStream fos = new FileOutputStream(file)) {
            fos.write(data.getBytes(), 0, data.length());
        } catch (FileNotFoundException e) {
            System.out.println("No such file: " + e);
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    @Override
    public String readData() {
        String result = null;
        File file = new File(name);

        try (FileReader reader = new FileReader(file)) {
            char[] buffer = new char[(int) file.length()];
            reader.read(buffer);
            result = new String(buffer);
        }  catch (FileNotFoundException e) {
            System.out.println("No such file: " + e);
        } catch (IOException e) {
            System.out.println(e);
        }

        return result;
    }
}
