package structural.decorator.pizza_maker;

public class PizzaPepperoni extends PizzaImpl {
    public PizzaPepperoni() {
        super("Pepperoni");
        addTopping("pepperoni");
    }
}
