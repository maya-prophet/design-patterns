package structural.decorator.pizza_maker;

import java.util.*;

public interface Pizza {

    List<String> getToppings();

    String getName();
}
