package structural.decorator.pizza_maker;

public class Main {
    public static void main(String[] args) {
        Pizza pizza1 = new PizzaWithExtraCheese(new PizzaHawaiian());
        Pizza pizza2 = new PizzaPepperoni();
        System.out.println(pizza1);
        System.out.println(pizza2);
    }
}
