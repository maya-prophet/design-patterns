package structural.decorator.pizza_maker;

import java.util.List;

public class PizzaWithExtraCheese implements Pizza {

    private List<String> toppings;
    private Pizza pizza;

    public PizzaWithExtraCheese(Pizza pizza) {
        this.pizza = pizza;
        toppings = pizza.getToppings();
        toppings.add("extra cheese");
    }

    @Override
    public List<String> getToppings() {
        return toppings;
    }

    @Override
    public String getName() {
        return pizza.getName();
    }

    @Override
    public String toString() {
        return "PizzaWithExtraCheese{" +
                "toppings=" + toppings +
                ", pizzaName=" + pizza.getName() +
                '}';
    }
}
