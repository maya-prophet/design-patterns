package structural.decorator.pizza_maker;

public class PizzaHawaiian extends PizzaImpl {
    public PizzaHawaiian() {
        super("Hawaiian");
        addTopping("ham");
        addTopping("pineapple");
    }
}
