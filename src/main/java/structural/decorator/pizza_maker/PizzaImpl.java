package structural.decorator.pizza_maker;

import java.util.*;

public abstract class PizzaImpl implements Pizza {

    private String name;
    private List<String> toppings;

    public PizzaImpl(String name) {
        this.name = name;
        toppings = new ArrayList<>();
        toppings.add("cheese");
        toppings.add("tomato");
    }

    public String getName() {
        return name;
    }

    public List<String> getToppings() {
        return toppings;
    }

    protected void addTopping(String topping) {
        toppings.add(topping);
    }

    @Override
    public String toString() {
        return "PizzaImpl{" +
                "name='" + name + '\'' +
                ", toppings=" + toppings +
                '}';
    }
}
