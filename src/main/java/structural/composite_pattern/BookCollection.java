package structural.composite_pattern;

import java.util.*;

public class BookCollection implements LibraryObject{
    private List<LibraryObject> libraryObjects = new ArrayList<>();

    public void addBook(LibraryObject libraryObject) {
        libraryObjects.add(libraryObject);
    }

    public void checkout() {
        libraryObjects.forEach(LibraryObject::checkout);
    }

    public void returnBook() {
        libraryObjects.forEach(LibraryObject::returnBook);
    }
}
