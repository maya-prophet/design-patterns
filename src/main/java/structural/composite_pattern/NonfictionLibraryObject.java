package structural.composite_pattern;

public class NonfictionLibraryObject extends Book {
    public NonfictionLibraryObject(String name) {
        super(name);
    }
}
