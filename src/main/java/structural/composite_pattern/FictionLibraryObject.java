package structural.composite_pattern;

public class FictionLibraryObject extends Book {
    boolean isAPlay;

    public FictionLibraryObject(String name, boolean isAPlay) {
        super(name);
        this.isAPlay = isAPlay;
    }


}
