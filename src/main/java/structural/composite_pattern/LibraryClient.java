package structural.composite_pattern;

public class LibraryClient {
    public static void main(String[] args) {
        LibraryObject book1 = new NonfictionLibraryObject("Invisible Women");
        LibraryObject book2 = new FictionLibraryObject("Rama", false);
        LibraryObject book3 = new FictionLibraryObject("Sandman", true);

        BookCollection bookCollection = new BookCollection();
        bookCollection.addBook(book1);
        bookCollection.addBook(book2);
        bookCollection.addBook(book3);

        checkoutBook(book1);
        checkoutBook(bookCollection);

        returnBook(book1);
        returnBook(bookCollection);


    }

    public static void checkoutBook(LibraryObject libraryObject) {
        libraryObject.checkout();
    }

    public static void returnBook(LibraryObject libraryObject) {
        libraryObject.returnBook();
    }
}
