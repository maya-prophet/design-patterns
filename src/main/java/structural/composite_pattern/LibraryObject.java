package structural.composite_pattern;

public interface LibraryObject {

    void checkout();
    void returnBook();

}
