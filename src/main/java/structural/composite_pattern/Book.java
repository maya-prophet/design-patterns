package structural.composite_pattern;

public abstract class Book implements LibraryObject {
    private String name;
    private boolean checkedOut;

    public Book() {
    }

    public Book(String name) {
        this.name = name;
        checkedOut = false;
    }

    public void checkout() {
        if (!checkedOut) {
            System.out.println("Checking out " + name + "\n");
            checkedOut = true;
        } else {
            System.out.println(name + " is already checked out\n");
        }
    }

    public void returnBook() {
        if (checkedOut) {
            System.out.println("Returning " + name + "\n");
            checkedOut = false;
        } else {
            System.out.println(name + " is not checked out\n");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }
}
