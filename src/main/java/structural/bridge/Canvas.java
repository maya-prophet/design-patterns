package structural.bridge;

/**
 * two separate hierarchies for type of button and size of it
 * with size interface being a property in button class
 */
public class Canvas {

    public static void main(String[] args) {
        Button checkboxButton = new CheckboxButton(new ElementSizeLarge());
        checkboxButton.draw();

        Button radioButton = new RadioButton(new ElementSizeMedium());
        radioButton.draw();

        Button dropdownButton = new DropdownButton(new ElementSizeSmall());
        dropdownButton.draw();
    }
}
