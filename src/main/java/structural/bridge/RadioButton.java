package structural.bridge;

public class RadioButton extends Button {

    private ElementSize elementSize;

    public RadioButton(ElementSize elementSize) {
        this.elementSize = elementSize;
    }

    @Override
    void draw() {
        elementSize.getSize();
        System.out.println("Drawing Radio Button");
    }
}
