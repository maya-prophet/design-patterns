package structural.bridge;


public class ElementSizeSmall implements ElementSize {
    @Override
    public void getSize() {
        System.out.println("Setting size to small...");
    }
}
