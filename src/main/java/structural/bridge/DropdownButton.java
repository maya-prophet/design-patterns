package structural.bridge;

public class DropdownButton extends Button {

    private ElementSize elementSize;

    public DropdownButton(ElementSize elementSize) {
        this.elementSize = elementSize;
    }

    @Override
    void draw() {
        elementSize.getSize();
        System.out.println("Drawing Dropdown Button");
    }
}
