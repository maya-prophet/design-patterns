package structural.bridge;

public class ElementSizeMedium implements ElementSize {

    @Override
    public void getSize() {
        System.out.println("Setting size to medium...");
    }
}
