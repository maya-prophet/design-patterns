package structural.bridge;

public class ElementSizeLarge implements ElementSize {

    @Override
    public void getSize() {
        System.out.println("Setting size to large...");
    }
}
