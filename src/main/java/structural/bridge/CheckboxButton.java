package structural.bridge;

public class CheckboxButton extends Button {

    private ElementSize elementSize;

    public CheckboxButton(ElementSize elementSize) {
        this.elementSize = elementSize;
    }

    @Override
    void draw() {
        elementSize.getSize();
        System.out.println("Drawing Checkbox Button");
    }
}
