package structural.facade;

public class GearStick {

    private int gear;

    public void changeGear(int gear) {
        this.gear = gear;
        System.out.println("Gear is changed to " + gear);
    }
}
