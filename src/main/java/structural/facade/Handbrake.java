package structural.facade;

public class Handbrake {
    public void pushDown() {
        System.out.println("Handbrake is pushed down");
    }

    public void liftUp() {
        System.out.println("Handbrake is lifted up");
    }
}
