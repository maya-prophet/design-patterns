package structural.facade;

public class CarDemo {
    public static void main(String[] args) {
        CarFacade car = new CarFacade();
        car.start();
    }
}
