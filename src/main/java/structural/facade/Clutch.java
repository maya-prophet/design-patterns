package structural.facade;

public class Clutch {

    public void press() {
        System.out.println("Clutch is pressed");
    }

    public void lift() {
        System.out.println("Clutch is lifted");
    }
}
