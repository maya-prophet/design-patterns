package structural.facade;

public class CarFacade {
    final private Ignition ignition;
    final private Clutch clutch;
    final private GearStick gearStick;
    final private Accelerator accelerator;
    final private Handbrake handbrake;

    public CarFacade() {
        this.ignition = new Ignition();
        this.clutch = new Clutch();
        this.gearStick = new GearStick();
        this.accelerator = new Accelerator();
        this.handbrake = new Handbrake();
    }

    public void start() {
        ignition.turnOn();
        clutch.press();
        gearStick.changeGear(1);
        accelerator.press();
        clutch.lift();
        handbrake.pushDown();
        accelerator.press();
        clutch.press();
    }
}
