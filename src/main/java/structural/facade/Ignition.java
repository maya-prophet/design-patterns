package structural.facade;

public class Ignition {

    public void turnOn() {
        System.out.println("Ignition is turned on");
    }
}
