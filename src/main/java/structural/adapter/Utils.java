package structural.adapter;

public class Utils {
    private Utils() {}

    public static StringBuilder getWarningMessage(City city) {
        StringBuilder stringBuilder = new StringBuilder("Warning! Current temperature in ");
        stringBuilder.append(city.getName());
        stringBuilder.append(" is ");
        stringBuilder.append(city.getTemperature());
        stringBuilder.append(" ");
        stringBuilder.append(city.getTemperatureScale());
        return stringBuilder;
    }

    public static String getOkMessage(City city) {
        return "Temperature in " + city.getName() + " is OK.";
    }
}
