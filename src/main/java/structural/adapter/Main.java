package structural.adapter;

public class Main {

    public static void main(String[] args) {
        WeatherWarnings weatherWarnings = new WeatherWarnings();

        NorthAmericanCity chicago = new NorthAmericanCity("Chicago", 16);
        weatherWarnings.postWarning(chicago);

        NorthAmericanCity portland = new NorthAmericanCity("Portland", 70);
        weatherWarnings.postWarning(portland);

        NorthAmericanCity phoenix = new NorthAmericanCity("Phoenix", 104);
        weatherWarnings.postWarning(phoenix);

        EuropeanCity moscow = new EuropeanCity("Moscow", 50);
        Adapter adapter = new Adapter(moscow);
        weatherWarnings.postWarning(adapter);
    }
}
