package structural.adapter;

public class WeatherWarnings {
    public static double MAX_TEMPERATURE = 100;
    public static double MIN_TEMPERATURE = 16;

    public void postWarning(City city) {
        if (city.getTemperature() >= MAX_TEMPERATURE || city.getTemperature() <= MIN_TEMPERATURE) {
            StringBuilder stringBuilder = Utils.getWarningMessage(city);
            System.out.println(stringBuilder);
        } else {
            System.out.println(Utils.getOkMessage(city));
        }
    }



}
