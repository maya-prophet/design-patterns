package structural.adapter;

public class NorthAmericanCity implements City {

    private String name;
    private double temperature;
    private boolean hasTemperatureWarning;

    public NorthAmericanCity(String name, double temperature) {
        this.name = name;
        this.temperature = temperature;
        hasTemperatureWarning = false;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getTemperature() {
        return temperature;
    }

    @Override
    public String getTemperatureScale() {
        return "Fahrenheit";
    }

    @Override
    public boolean getHasWeatherWarning() {
        return hasTemperatureWarning;
    }

    @Override
    public void setHasWeatherWarning(boolean hasWeatherWarning) {
        this.hasTemperatureWarning = hasWeatherWarning;
    }
}
