package creational.singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static creational.singleton.Constants.*;

public enum DbConnection {
    INSTANCE;

    private Connection connection;

    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                                           USER, PASSWORD);
    }

}
