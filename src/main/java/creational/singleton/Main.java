package creational.singleton;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        PrintSpooler spooler = PrintSpooler.getInstance();

        try (Connection connection = DbConnection.INSTANCE.newConnection()){

        } catch (SQLException ex) {
            System.out.println("Exception occurred connecting to DB: " + ex.getMessage());
        }
    }
}
