package creational.singleton;

//double-checked locking (DCL)
public class PrintSpooler {

    private static volatile PrintSpooler instance;

    private PrintSpooler() {
    }

    public static PrintSpooler getInstance() {
        PrintSpooler localInstance = instance;
        if (localInstance != null) {
            return localInstance;
        }
        synchronized(PrintSpooler.class) {
            if (instance == null) {
                instance = new PrintSpooler();
            }
            return instance;
        }
    }
}
