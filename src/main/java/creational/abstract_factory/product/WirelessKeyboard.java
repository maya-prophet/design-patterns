package creational.abstract_factory.product;

public class WirelessKeyboard implements Keyboard {
    @Override
    public void printOutDescription() {
        System.out.println("Wireless Keyboard is created");
    }
}
