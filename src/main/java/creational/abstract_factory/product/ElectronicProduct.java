package creational.abstract_factory.product;

public interface ElectronicProduct {

    void printOutDescription();

}
