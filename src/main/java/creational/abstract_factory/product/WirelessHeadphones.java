package creational.abstract_factory.product;

public class WirelessHeadphones implements Headphones {


    @Override
    public void printOutDescription() {
        System.out.println("Wireless Headphones are created");
    }
}
