package creational.abstract_factory.product;

public class WiredHeadphones implements Headphones {
    @Override
    public void printOutDescription() {
        System.out.println("Wired Headphones are created");
    }
}
