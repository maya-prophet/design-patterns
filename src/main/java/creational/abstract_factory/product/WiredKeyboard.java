package creational.abstract_factory.product;

public class WiredKeyboard implements Keyboard {

    @Override
    public void printOutDescription() {
        System.out.println("Wired Keyboard is created");
    }
}
