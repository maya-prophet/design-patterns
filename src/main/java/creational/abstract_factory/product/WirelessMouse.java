package creational.abstract_factory.product;

public class WirelessMouse implements Mouse {
    @Override
    public void printOutDescription() {
        System.out.println("Wireless Mouse is created");
    }
}
