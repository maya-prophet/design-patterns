package creational.abstract_factory.product;

public class WiredMouse implements Mouse {

    @Override
    public void printOutDescription() {
        System.out.println("Wired Mouse is created");
    }
}
