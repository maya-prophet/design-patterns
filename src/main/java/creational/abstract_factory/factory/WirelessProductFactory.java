package creational.abstract_factory.factory;

import creational.abstract_factory.product.*;

public class WirelessProductFactory extends ProductFactory {

    @Override
    public ElectronicProduct createProduct(ProductType type) {
        if (type.equals(ProductType.KEYBOARD)) {
            return new WirelessKeyboard();
        } else if (type.equals(ProductType.MOUSE)) {
            return new WirelessMouse();
        } else
            return new WirelessHeadphones();
    }
}
