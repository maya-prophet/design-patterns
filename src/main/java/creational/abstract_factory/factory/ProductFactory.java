package creational.abstract_factory.factory;

import creational.abstract_factory.product.ElectronicProduct;

public abstract class ProductFactory {

    public abstract ElectronicProduct createProduct(ProductType type);

    public enum ProductType {
        KEYBOARD,
        MOUSE,
        HEADPHONES
    }
}
