package creational.abstract_factory.factory;

import creational.abstract_factory.product.ElectronicProduct;
import creational.abstract_factory.product.WiredHeadphones;
import creational.abstract_factory.product.WiredKeyboard;
import creational.abstract_factory.product.WiredMouse;

public class WiredProductFactory extends ProductFactory {

    @Override
    public ElectronicProduct createProduct(ProductType type) {
        if (type.equals(ProductType.KEYBOARD)) {
            return new WiredKeyboard();
        } else if (type.equals(ProductType.MOUSE)) {
            return new WiredMouse();
        } else
            return new WiredHeadphones();
    }
}
