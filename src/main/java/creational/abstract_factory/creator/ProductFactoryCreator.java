package creational.abstract_factory.creator;

import creational.abstract_factory.factory.ProductFactory;
import creational.abstract_factory.factory.WiredProductFactory;
import creational.abstract_factory.factory.WirelessProductFactory;

public class ProductFactoryCreator {

    private ProductFactoryCreator() {}

    public static ProductFactory getFactory(FactoryType type) {
        if (type.equals(FactoryType.WIRELESS)) {
            return new WirelessProductFactory();
        } else
            return new WiredProductFactory();
    }

    public enum FactoryType {
        WIRELESS,
        WIRED
    }
}
