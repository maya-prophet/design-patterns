package creational.abstract_factory;

import creational.abstract_factory.creator.ProductFactoryCreator;
import creational.abstract_factory.factory.ProductFactory;
import creational.abstract_factory.product.Headphones;
import creational.abstract_factory.product.Keyboard;
import creational.abstract_factory.product.Mouse;


public class Main {
    public static void main(String[] args) {
        createProductSet(ProductFactoryCreator.FactoryType.WIRED);
    }

    public static void createProductSet(ProductFactoryCreator.FactoryType type) {
        ProductFactory factory = ProductFactoryCreator.getFactory(type);

        Keyboard keyboard = (Keyboard) factory.createProduct(ProductFactory.ProductType.KEYBOARD);
        Mouse mouse = (Mouse) factory.createProduct(ProductFactory.ProductType.MOUSE);
        Headphones headphones = (Headphones) factory.createProduct(ProductFactory.ProductType.HEADPHONES);

        keyboard.printOutDescription();
        mouse.printOutDescription();
        headphones.printOutDescription();
    }
}
