package creational.prototype;

public class Dog implements Cloneable {
    private Breed breed;
    private Person owner;
    private String name;
    private int age;

    public Dog(Breed breed, Person owner, String name, int age) {
        this.breed = breed;
        this.owner = owner;
        this.name = name;
        this.age = age;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public Dog clone() {
        try {
            Dog dog = (Dog) super.clone();
            dog.setOwner(owner.clone());
            return dog;
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String toString() {
        return "Dog{" +
                "breed=" + breed +
                ", owner=" + owner +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public enum Breed {
        BORDER_COLLIE,
        SHEPHERD,
        XOLOITZCUINTLE
    }
}
