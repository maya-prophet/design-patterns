package creational.prototype;

public class DogBuilder {
    private Dog.Breed breed;
    private Person owner;
    private String name;
    private int age;

    public DogBuilder setBreed(Dog.Breed breed) {
        this.breed = breed;
        return this;
    }

    public DogBuilder setOwner(Person owner) {
        this.owner = owner;
        return this;
    }

    public DogBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public DogBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    public Dog createDog() {
        return new Dog(breed, owner, name, age);
    }
}