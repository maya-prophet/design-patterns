package creational.prototype;

public class Main {
    public static void main(String[] args) {
        Dog dog1 = new DogBuilder()
                .setAge(3)
                .setName("Angie")
                .setBreed(Dog.Breed.XOLOITZCUINTLE)
                .setOwner(new Person("Bryan"))
                .createDog();

        Dog dog2 = dog1.clone();
        dog2.setName("Bolo");
        System.out.println(dog1);
        System.out.println(dog2);
    }
}
