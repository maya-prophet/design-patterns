package creational.factory_method;

public class Main {

    public static StayManager stayManager = new StayManager();
    public static void main(String[] args) {
        stayManager.addNewStay("hotel");
        stayManager.addNewStay("motel");
        stayManager.addNewStay("inn");
        System.out.println(StayManager.stays);
    }
}
