package creational.factory_method;

public class Motel extends Stay {

    private Room[] floorPlan;

    public Motel(){}

    public Motel(int numberOfRoomsPerFloor) {
        super(numberOfRoomsPerFloor);
        setFloorPlan();
    }

    public Room[] getFloorPlan() {
        return floorPlan;
    }

    private void setFloorPlan() {
        floorPlan = new Room[getNumberOfRoomsPerFloor()];
    }
}
