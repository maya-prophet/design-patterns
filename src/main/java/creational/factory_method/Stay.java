package creational.factory_method;

import java.util.List;

public abstract class Stay {
    private int numberOfRoomsPerFloor;

    public Stay() {}

    public Stay(int numberOfRoomsPerFloor) {
        this.numberOfRoomsPerFloor = numberOfRoomsPerFloor;
    }

    public int getNumberOfRoomsPerFloor() {
        return numberOfRoomsPerFloor;
    }

    public void setNumberOfRoomsPerFloor(int numberOfRoomsPerFloor) {
        this.numberOfRoomsPerFloor = numberOfRoomsPerFloor;
    }

    public double getCurrentRating(List<Integer> ratings) {
        return ratings.stream().mapToInt(Integer::valueOf).average().orElse(0);
    }

    record Room(String type) {
    }
}
