package creational.factory_method;

import java.util.HashMap;
import java.util.Map;

public class Hotel extends Stay {
    private Map<Integer, Room[]> floorPlan;
    private int numberOfFloors;

    public Hotel() {}

    public Hotel(int numberOfFloors, int numberOfRoomsPerFloor) {
        super(numberOfRoomsPerFloor);
        this.numberOfFloors = numberOfFloors;
        setFloorPlan();
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public Map<Integer, Room[]> getFloorPlan() {
        return floorPlan;
    }

    private void setFloorPlan() {
        floorPlan = new HashMap<>();
        for (int i = 1; i <= numberOfFloors; i++) {
            floorPlan.put(i, new Room[getNumberOfRoomsPerFloor()]);
        }
    }
}
