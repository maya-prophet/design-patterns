package creational.factory_method;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class StayManager {

    public static final List<Stay> stays = new ArrayList<>();

    public void addNewStay(String type) {
        try {
            Stay stay = createStay(type).orElseThrow();
            stays.add(stay);
        } catch (NoSuchElementException ex) {
            System.out.println("No Stay type: " + type);
        }
    }

    //factory method
    private Optional<Stay> createStay(String type) {
        if ("hotel".equalsIgnoreCase(type)) {
            return Optional.of(new Hotel());
        } else if ("motel".equalsIgnoreCase(type)) {
            return Optional.of(new Motel());
        } else return Optional.empty();
    }
}
