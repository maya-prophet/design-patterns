package creational.builder;

import java.awt.*;

public class Wagon {
    private final Dimension dimensions;
    private final double tare;
    private final double loadCapacity;
    private Color color;
    private final int numberOfWindows;
    private final int numberOfDoors;

    public Wagon(Dimension dimensions, double tare, double loadCapacity, Color color, int numberOfWindows, int numberOfDoors) {
        this.dimensions = dimensions;
        this.tare = tare;
        this.loadCapacity = loadCapacity;
        this.color = color;
        this.numberOfWindows = numberOfWindows;
        this.numberOfDoors = numberOfDoors;
    }
}
