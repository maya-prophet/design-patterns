package creational.builder;

import java.awt.*;
import java.util.ArrayList;

public class TrainManager {

    public static void main(String[] args) {
        ArrayList<Wagon> wagons = new WagonListBuilder().addList()
                                                        .addWagon()
                                                            .setColor(Color.BLUE)
                                                            .setLoadCapacity(50)
                                                            .addWagonToList()
                                                        .addWagon()
                                                            .setTare(15)
                                                            .addWagonToList()
                                                        .buildList();
        Train train = new Train(wagons);
    }
}
