package creational.builder;

import java.awt.*;

public class WagonBuilder {
    private Dimension dimensions;
    private double tare;
    private double loadCapacity;
    private Color color;
    private int numberOfWindows;
    private int numberOfDoors;
    private final WagonListBuilder wagonListBuilder;

    public WagonBuilder(WagonListBuilder wagonListBuilder) {
        this.wagonListBuilder = wagonListBuilder;
    }

    public WagonBuilder setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;
        return this;
    }

    public WagonBuilder setTare(double tare) {
        this.tare = tare;
        return this;
    }

    public WagonBuilder setLoadCapacity(double loadCapacity) {
        this.loadCapacity = loadCapacity;
        return this;
    }

    public WagonBuilder setColor(Color color) {
        this.color = color;
        return this;
    }

    public WagonBuilder setNumberOfWindows(int numberOfWindows) {
        this.numberOfWindows = numberOfWindows;
        return this;
    }

    public WagonBuilder setNumberOfDoors(int numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
        return this;
    }

    public Wagon createWagon() {
        return new Wagon(dimensions, tare, loadCapacity, color, numberOfWindows, numberOfDoors);
    }

    public WagonListBuilder addWagonToList() {
        Wagon wagon = createWagon();
        this.wagonListBuilder.addWagon(wagon);
        return this.wagonListBuilder;
    }
}