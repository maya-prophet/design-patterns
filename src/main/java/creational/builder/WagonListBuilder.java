package creational.builder;

import java.util.ArrayList;

public class WagonListBuilder {

    private ArrayList<Wagon> listOfWagons;

    public WagonListBuilder addList() {
        this.listOfWagons = new ArrayList<>();
        return this;
    }

    public WagonListBuilder addWagon(Wagon wagon) {
        listOfWagons.add(wagon);
        return this;
    }

    public WagonBuilder addWagon() {
        return new WagonBuilder(this);
    }

    public ArrayList<Wagon> buildList() {
        return listOfWagons;
    }


}
