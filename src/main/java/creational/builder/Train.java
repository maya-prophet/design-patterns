package creational.builder;

import java.util.ArrayList;

public class Train {

    private final ArrayList<Wagon> listOfWagons;

    public Train(ArrayList<Wagon> listOfWagons) {
        this.listOfWagons = listOfWagons;
    }
}
